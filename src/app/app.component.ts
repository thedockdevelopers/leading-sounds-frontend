import { Component } from '@angular/core';
import * as AOS from 'aos';
import { Router, NavigationEnd} from '@angular/router'; 
import { UtilityService } from './core/services/utility.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Leading Sounds';
  isLoaded: boolean = false;

  constructor(
    private utilityService: UtilityService,
    private router: Router
  ) {
    
    this.utilityService.globalConfigObservable.subscribe((globalConfigLoaded: any) => {
      if (globalConfigLoaded) {
        this.isLoaded = true;
        this.router.events.subscribe((event) => {
          if (event instanceof NavigationEnd){
             //scroll to top
             window.scrollTo(0,0);
          }
       });
      }
    });
  }

  ngOnInit() {
    AOS.init();
    this.utilityService.getGlobalConfigurations();
    this.utilityService.setUpCartItems();
  }
}
