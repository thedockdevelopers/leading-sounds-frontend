import { NgModule } from '@angular/core';
import { PublicationRoutingModule } from './publication-routing.module';
import { FaqsItemModule } from '../../components';

@NgModule({
  declarations: [
  ],
  imports: [
    PublicationRoutingModule,
    FaqsItemModule
  ]
})
export class PublicationModule { }
