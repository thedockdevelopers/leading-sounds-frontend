import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PublicationComponent } from './publication.component';
import { FaqsItemModule } from '../../components';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: PublicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, FaqsItemModule, SharedModule, FormsModule],
  exports: [RouterModule],
  declarations: [PublicationComponent]
})
export class PublicationRoutingModule { }
