import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})
export class PublicationComponent implements OnInit {

  publicationData: any;
  whyPublishData: any;
  globalConfig: any;
  faqsData:any;
  stepsData: any;
  publicationPackagesData: any;
  publicationPackagesSubs: Subscription = new Subscription();
  name: string | undefined;
  email: string | undefined ;
  phone: number | undefined;
  instaUsername: string | undefined;
  spotifyLink: string | undefined;
  ytChannelLink: string | undefined;
  aboutClient: string | undefined;
  isError: boolean = false;
  
  constructor(
    private utilityService: UtilityService,
    private router: Router) { }

  ngOnInit(): void {
    this.globalConfig = this.utilityService.getGlobalConfig();
    this.publicationPackagesSubs = this.utilityService.getPublication().subscribe((response: any) => {
      if (response && response.data) {
        this.publicationData = response.data.attributes;
        this.publicationPackagesData = response.data.attributes.Packages?.data;
        this.whyPublishData = response.data.attributes.WhyPromotes?.data;
        this.faqsData = response.data.attributes.Faqs?.data;
        this.stepsData = response.data.attributes.Steps?.data.slice(0,3);
      }
    });
  }

  submit(e: any) {
    let userInfo = {
      name: this.name,
      email: this.email,
      phone: this.phone,
      instaUsername: this.instaUsername,
      spotifyLink: this.spotifyLink,
      ytChannelLink: this.ytChannelLink,
      aboutClient: this.aboutClient
    }
    let contactForm: any = document.getElementById('publication-form');
    this.utilityService.postPublicationContactData(userInfo).subscribe((response: any) => {
      if(response && response.success) {
        this.isError = false;
        let successDiv: any = document.getElementById('publication-success');
        successDiv.style.display = 'block';
        contactForm.reset();
      }
      else {
        this.isError = true;
      }
    });
    e.preventDefault();
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/publication/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }
 
  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  ngOnDestroy() {
    if(this.publicationPackagesSubs) {
      this.publicationPackagesSubs.unsubscribe();
    }
  }

}
