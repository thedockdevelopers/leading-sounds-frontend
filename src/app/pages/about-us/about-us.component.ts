import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit, OnDestroy {

  aboutUsSubs: Subscription = new Subscription();
  aboutUsData: any;
  aboutUsItems: any;
  teamData: any;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit(): void {
    this.aboutUsSubs = this.utilityService.getAboutUs().subscribe((response) => {
      if(response && response.data) {
        this.aboutUsData = response.data.attributes;
        this.aboutUsItems = response.data.attributes.AboutUs?.data;
        this.teamData = response.data.attributes.Team?.data;
      }
    });
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  openPromo(promoLink: string) {
    window.location.href = promoLink;
  }

  ngOnDestroy(): void {
      if(this.aboutUsSubs) {
        this.aboutUsSubs.unsubscribe();
      }
  }
}
