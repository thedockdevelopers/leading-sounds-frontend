import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TAndCRoutingModule } from './t-and-c-routing.module';
import { TAndCComponent } from './t-and-c.component';


@NgModule({
  declarations: [
    TAndCComponent
  ],
  imports: [
    CommonModule,
    TAndCRoutingModule
  ]
})
export class TAndCModule { }
