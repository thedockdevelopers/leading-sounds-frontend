import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TAndCComponent } from './t-and-c.component';

const routes: Routes = [
  {
    path: '',
    component: TAndCComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TAndCRoutingModule { }
