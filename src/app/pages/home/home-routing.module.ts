import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from '../../shared/shared.module';
import { TeaserModule, PartnersModule, VideoModule, AboutUsModule, OurWorkModule, TestimonialsModule, WhyPromoteModule, ServicesModule } from '../../components';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, TeaserModule, PartnersModule, VideoModule, AboutUsModule, WhyPromoteModule, OurWorkModule, TestimonialsModule, ServicesModule, SharedModule],
  exports: [RouterModule],
  declarations: [HomeComponent]
})
export class HomeRoutingModule { }
