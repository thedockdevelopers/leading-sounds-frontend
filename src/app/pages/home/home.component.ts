import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoaded: boolean = false;

  constructor(
    private utilityService: UtilityService) {
  }

  ngOnInit(): void {
    this.utilityService.getHomePage().subscribe((response: any) => {
      if(response && response.data && response.data) {
        this.utilityService.setHomePageData(response.data.attributes);
        this.isLoaded = true;
      }
    })
  }
}