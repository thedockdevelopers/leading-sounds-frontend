import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  firstName: string | undefined;
  lastName: string | undefined ;
  email: string | undefined ;
  phone: number | undefined;
  instagram: boolean = false;
  spotify: boolean = false;
  youtube: boolean = false;
  soundcloud: boolean = false;
  webDesign: boolean = false;
  webDevelopment: boolean = false;
  aboutClient: string | undefined ;
  isError: boolean = false;

  constructor(
    private utilityService: UtilityService) { }

  ngOnInit(): void {
  }

  submit(e: any) {
    let services = {
      Instgram: this.instagram,
      Spotify: this.spotify,
      Youtube: this.youtube,
      SoundCloud: this.soundcloud,
      WebDesign: this.webDesign,
      WebDevelopment: this.webDevelopment
    };
    let userInfo = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
      services: services,
      aboutClient: this.aboutClient
    }
    let contactForm: any = document.getElementById('contact-form');
    this.utilityService.postContactUsData(userInfo).subscribe((response: any) => {
      if(response && response.success) {
        this.isError = false;
        let successDiv: any = document.getElementById('contact-us-success');
        successDiv.style.display = 'block';
        contactForm.reset();
      }
      else {
        this.isError = true;
      }
    });
    e.preventDefault();
  }

}
