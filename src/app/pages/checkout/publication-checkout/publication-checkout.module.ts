import { NgModule } from '@angular/core';
import { PublicationCheckoutRoutingModule } from './publication-checkout-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    PublicationCheckoutRoutingModule
  ]
})
export class PublicationCheckoutModule { }
