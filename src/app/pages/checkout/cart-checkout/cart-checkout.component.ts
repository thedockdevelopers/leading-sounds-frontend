import { Component, HostListener, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';
import { UserInfoModel } from '../../../shared';

declare var StripeCheckout: StripeCheckoutStatic;

@Component({
  selector: 'app-cart-checkout',
  templateUrl: './cart-checkout.component.html',
  styleUrls: ['./cart-checkout.component.scss']
})
export class CartCheckoutComponent implements OnInit {

  handler: StripeCheckoutHandler | undefined;
  confirmation: any;
  loading = false;
  globalConfig: any;
  isLoaded: boolean = false;
  infoSubmitted: boolean = false;
  billingSubmitted: boolean = false;
  repostPackagesSubs: Subscription = new Subscription();
  couponDataSubs: Subscription = new Subscription();
  itemArrFromCookies: any;
  cartItems: Array<any> = new Array<any>();
  noOfItem: number = 0;
  couponCode: any;
  totalAmount: number = 0;
  discountedAmount: number = 0;
  userModel: UserInfoModel = new UserInfoModel();
  firstName: any;
  lastName: string | undefined ;
  email: string | undefined ;
  phone: number | undefined;
  instaUsername: string | undefined;
  address: string | undefined;
  city: string | undefined;
  state: string | undefined;
  zipCode: string | undefined;
  country: string | undefined;
  cardOverlayOpened = false;
  paymentCompleted = false;
  cartObject: any;
  paymentError: string | undefined;
  currency: string = 'usd';
  

  constructor(
    private utilityService: UtilityService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.globalConfig = this.utilityService.getGlobalConfig();
    this.itemArrFromCookies = this.cookieService.get('ls_cart_checkout') ? JSON.parse(this.cookieService.get('ls_cart_checkout')) : null;
    if(!this.itemArrFromCookies) {
      window.location.href = '/cart';
    }
    else {
      let thi = this;
      this.handler = StripeCheckout.configure({
        key: this.utilityService.getPublishableKey(),
        image: this.getImageUrl(this.globalConfig.TertiaryLogo.data.attributes.url),
        locale: 'auto',
        token: function (stripeToken: any) {
          thi.storePaymentReceipt(stripeToken.id);
        }
      })
      this.setCartArray();
      if(this.itemArrFromCookies.couponCode) {
        this.getCouponData();
      }
    }
  }

  storePaymentReceipt(transactionId: string) {
    this.cartObject['transactionId'] = transactionId;
    this.utilityService.postPaymentData(this.cartObject).subscribe((response: any) => {
      if(response) {
        if(response && response.isSuccess) {
          this.cardOverlayOpened = false;
          this.cookieService.delete('ls_rp_items_in_cart', '/');
          this.cookieService.delete('ls_cart_checkout', '/');
          window.location.href = '/payment-status?id=' + response.transactionId
        }
        else {
          this.cardOverlayOpened = false;
          this.paymentError = response.message;
        }
      }
    },(error) => {
      this.cardOverlayOpened = false;
      this.paymentError = 'We are facing some technical issue. Please try again later.'
    }
    )
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  setCartArray() {
    this.repostPackagesSubs = this.utilityService.getRepostPackagesData().subscribe((response: any) => {
      if (response && response.data && response.data.length > 0) {
        this.isLoaded = true;
        let repostPackagesData = response.data;
        this.couponCode = this.itemArrFromCookies.couponCode;
        this.itemArrFromCookies.cartArr.forEach((item: any) => {
          for(let i = 0; i < repostPackagesData.length; i++ ) {
            if(repostPackagesData[i].id === item.id) {
              repostPackagesData[i].BuyCounter = item.BuyCounter;
              this.noOfItem += item.BuyCounter;
              this.totalAmount += (repostPackagesData[i].attributes.Price*item.BuyCounter);
              let cartObj = {
                id: repostPackagesData[i].id,
                name: repostPackagesData[i].attributes.Name,
                price: repostPackagesData[i].attributes.Price,
                buyCounter: repostPackagesData[i].BuyCounter
              };
              this.cartItems.push(cartObj);
            }
          }
        });
        this.utilityService.cartSubject.next(this.noOfItem);
      }
    });
  }

  getCouponData() {
    this.couponDataSubs = this.utilityService.getCouponData(this.itemArrFromCookies.couponCode).subscribe((response: any) => {
      if(response) {
        if(response.isValid) {
          this.couponCode = this.itemArrFromCookies.couponCode;
          this.discountedAmount = response.amountOff;
        }
        else {
          this.couponCode = '';
          this.discountedAmount = 0;
        }
      }
    });
  }

  next(infoSubmitted: boolean, billingSubmitted: boolean) {
    this.infoSubmitted = infoSubmitted;
    this.billingSubmitted = billingSubmitted;
  }

  getFinalAmount() {
    return this.totalAmount - this.discountedAmount;
  }

  checkout(e: any) {
    this.cardOverlayOpened = true;
    this.paymentError = '';
    let userInfo: UserInfoModel = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
      instaUsername: this.instaUsername,
      address: this.address,
      city: this.city,
      state: this.state,
      zipCode: this.zipCode,
      country: this.country
    }
    let couponData = {
      totalAmount: this.totalAmount,
      couponCode: this.couponCode,
      discountedAmount: this.discountedAmount
    }
    this.cartObject = {
      category: 'RepostPackages',
      cartItems: this.cartItems,
      finalAmount: this.getFinalAmount(),
      userInfo: userInfo,
      currency: 'usd',
      description: 'Payment for Repost Campaign',
      couponData: couponData
    };
    this.handler?.open({
      name: 'Leading Sounds',
      description: 'Payment for Repost Campaign',
      amount: Math.round(this.getFinalAmount()*100),
      email: this.email,
      currency: this.currency,
      
    });
    e.preventDefault();
  }

  @HostListener('window:popstate')
  onPopstate() {
    this.handler?.close();
  }
  
  convertToInr() {
    this.currency = 'inr';
    // Logic to convert usd to inr
  }

  ngOnDestroy() {
    if(this.repostPackagesSubs) {
      this.repostPackagesSubs.unsubscribe();
    }
  }

}
