import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CartCheckoutComponent } from './cart-checkout.component';

const routes: Routes = [
  {
    path: '',
    component: CartCheckoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule, FormsModule],
  exports: [RouterModule],
  declarations: [
    CartCheckoutComponent
  ]
})
export class CartCheckoutRoutingModule { }
