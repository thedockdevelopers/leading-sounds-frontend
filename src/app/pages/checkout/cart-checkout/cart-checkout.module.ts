import { NgModule } from '@angular/core';
import { CartCheckoutRoutingModule } from './cart-checkout-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CartCheckoutRoutingModule
  ]
})
export class CartCheckoutModule { }
