import { NgModule } from '@angular/core';
import { InstagramCheckoutRoutingModule } from './instagram-checkout-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    InstagramCheckoutRoutingModule
  ]
})
export class InstagramCheckoutModule { }
