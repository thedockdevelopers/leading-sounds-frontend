import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { InstagramCheckoutComponent } from './instagram-checkout.component';

const routes: Routes = [
  {
    path: '',
    component: InstagramCheckoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, FormsModule, SharedModule],
  exports: [RouterModule],
  declarations: [InstagramCheckoutComponent]
})
export class InstagramCheckoutRoutingModule { }
