import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';
import { UserInfoModel } from 'src/app/shared';

@Component({
  selector: 'app-spotify-checkout',
  templateUrl: './spotify-checkout.component.html',
  styleUrls: ['./spotify-checkout.component.scss']
})
export class SpotifyCheckoutComponent implements OnInit {

  globalConfig: any;
  handler: StripeCheckoutHandler | undefined;
  isLoaded: boolean = false;
  infoSubmitted: boolean = false;
  billingSubmitted: boolean = false;
  spotifyPackagesSubs: Subscription = new Subscription();
  firstName: any;
  lastName: string | undefined ;
  email: string | undefined ;
  phone: number | undefined;
  spotifyUserName: string | undefined;
  address: string | undefined;
  city: string | undefined;
  state: string | undefined;
  zipCode: string | undefined;
  country: string | undefined;
  cardOverlayOpened = false;
  paymentCompleted = false;
  cartObject: any;
  paymentError: string | undefined;
  currency: string = 'usd';
  currentPackageData: any;
  bulletPoints: any;
  spotifyPackagesData: any;
  packageId: string | undefined;

  constructor(
    private utilityService: UtilityService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.queryParams.subscribe(params => {
      this.packageId = params['id'];
  });}

  ngOnInit(): void {
    this.globalConfig = this.utilityService.getGlobalConfig();
    this.spotifyPackagesSubs = this.utilityService.getSpotifyPackagesData().subscribe((response: any) => {
      if (response && response.data && response.data.length > 0) {
        this.spotifyPackagesData = response.data;
        this.currentPackageData = this.spotifyPackagesData.filter((data:any) => data.id == this.packageId)[0];
        if(!this.currentPackageData) {
          window.location.href = '/404';
        }
        this.bulletPoints = Object.values(this.currentPackageData.attributes.BulletPoints);
        this.isLoaded = true;
        let thi = this;
        this.handler = StripeCheckout.configure({
          key: this.utilityService.getPublishableKey(),
          image: this.getImageUrl(this.globalConfig.TertiaryLogo.data.attributes.url),
          locale: 'auto',
          token: function (stripeToken: any) {
            thi.storePaymentReceipt(stripeToken.id);
          }
        })
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  openPackage() {
    let e: any = document.getElementById("changeCampaign");
    window.location.href = '/spotify/auth/signup?id=' + e.value;
  }

  storePaymentReceipt(transactionId: string) {
    this.cartObject['transactionId'] = transactionId;
    this.utilityService.postPaymentData(this.cartObject).subscribe((response: any) => {
      if(response) {
        if(response && response.isSuccess) {
          this.cardOverlayOpened = false;
          window.location.href = '/payment-status?id=' + response.transactionId
        }
        else {
          this.cardOverlayOpened = false;
          this.paymentError = response.message;
        }
      }
    },(error) => {
      this.cardOverlayOpened = false;
      this.paymentError = 'We are facing some technical issue. Please try again later.'
    }
    )
  }

  next(infoSubmitted: boolean, billingSubmitted: boolean) {
    this.infoSubmitted = infoSubmitted;
    this.billingSubmitted = billingSubmitted;
  }

  getFinalAmount() {
    return this.currentPackageData.attributes.Price;
  }

  checkout(e: any) {
    this.cardOverlayOpened = true;
    this.paymentError = '';
    let userInfo: UserInfoModel = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
      spotifyUserName: this.spotifyUserName,
      address: this.address,
      city: this.city,
      state: this.state,
      zipCode: this.zipCode,
      country: this.country
    }
    this.cartObject = {
      category: 'SpotifyPackages',
      cartItems: this.currentPackageData,
      finalAmount: this.getFinalAmount(),
      userInfo: userInfo,
      currency: 'usd',
      description: 'Payment for Spotify Campaign'
    };
    this.handler?.open({
      name: 'Leading Sounds',
      description: 'Payment for Spotify Campaign',
      amount: Math.round(this.getFinalAmount()*100),
      email: this.email,
      currency: this.currency,
      
    });
    e.preventDefault();
  }

  @HostListener('window:popstate')
  onPopstate() {
    this.handler?.close();
  }
  
  convertToInr() {
    this.currency = 'inr';
    // Logic to convert usd to inr
  }

  ngOnDestroy() {
    if(this.spotifyPackagesSubs) {
      this.spotifyPackagesSubs.unsubscribe();
    }
  }

}
