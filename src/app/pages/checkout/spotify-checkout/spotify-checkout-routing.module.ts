import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { SpotifyCheckoutComponent } from './spotify-checkout.component';

const routes: Routes = [
  {
    path: '',
    component: SpotifyCheckoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, FormsModule, SharedModule],
  exports: [RouterModule],
  declarations: [SpotifyCheckoutComponent]
})
export class SpotifyCheckoutRoutingModule { }
