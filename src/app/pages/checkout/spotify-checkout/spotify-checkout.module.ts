import { NgModule } from '@angular/core';
import { SpotifyCheckoutRoutingModule } from './spotify-checkout-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    SpotifyCheckoutRoutingModule
  ]
})
export class SpotifyCheckoutModule { }
