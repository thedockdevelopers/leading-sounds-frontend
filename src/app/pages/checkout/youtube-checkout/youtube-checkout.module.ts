import { NgModule } from '@angular/core';
import { YoutubeCheckoutRoutingModule } from './youtube-checkout-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    YoutubeCheckoutRoutingModule
  ]
})
export class YoutubeCheckoutModule { }
