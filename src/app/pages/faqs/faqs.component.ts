import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {

  faqsData: any;

  constructor(private utilityService: UtilityService) { }

  ngOnInit(): void {
    this.utilityService.getFaqsData().subscribe((response: any) => {
      if(response && response.data && response.data.length > 0) {
        this.faqsData = response.data;
      }
    });
  }

}
