import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaqsItemModule } from '../../components';
import { FaqsComponent } from './faqs.component';

const routes: Routes = [
  {
    path: '',
    component: FaqsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, FaqsItemModule],
  exports: [RouterModule],
  declarations: [FaqsComponent]
})
export class FaqsRoutingModule { }
