import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { SpotifyPackageComponent } from './spotify-package.component';

const routes: Routes = [
  {
    path: '',
    component: SpotifyPackageComponent
  }
];

@NgModule({
  declarations: [SpotifyPackageComponent],
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule]
})
export class SpotifyPackageRoutingModule { }
