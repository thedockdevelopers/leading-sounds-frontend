import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-spotify-package',
  templateUrl: './spotify-package.component.html',
  styleUrls: ['./spotify-package.component.scss']
})
export class SpotifyPackageComponent implements OnInit {

  instaPackagesSubs: Subscription = new Subscription();
  packageId: any;
  currentPackageData: any;
  remainingPackagesData: any;
  bulletPoints: any;
  isLoaded: boolean = false;

  constructor(
    private utilityService: UtilityService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.queryParams.subscribe(params => {
      this.packageId = params['id'];
  });
   }

  ngOnInit(): void {
    this.instaPackagesSubs = this.utilityService.getSpotifyPackagesData().subscribe((response: any) => {
      if (response && response.data && response.data.length > 0) {
        let spotifyPackagesData = response.data;
        this.currentPackageData = spotifyPackagesData.filter((data:any) => data.id == this.packageId)[0];
        if(!this.currentPackageData) {
          window.location.href = '/404';
        }
        this.isLoaded = true;
        this.bulletPoints = Object.values(this.currentPackageData.attributes.BulletPoints);
        this.remainingPackagesData = spotifyPackagesData.filter((data:any) => data.id != this.packageId);
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/spotify/campaign/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }

  ngOnDestroy() {
    if(this.instaPackagesSubs) {
      this.instaPackagesSubs.unsubscribe();
    }
  }

  buyNow(id: number) {
    let checkoutLink = '/spotify/auth/signup';
    this.router.navigate([checkoutLink], { queryParams: { id: id } });
  }

}
