import { NgModule } from '@angular/core';
import { SpotifyPackageRoutingModule } from './spotify-package-routing.module';


@NgModule({
  declarations: [
  ],
  imports: [
    SpotifyPackageRoutingModule
  ]
})
export class SpotifyPackageModule { }
