import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-publication-package',
  templateUrl: './publication-package.component.html',
  styleUrls: ['./publication-package.component.scss']
})
export class PublicationPackageComponent implements OnInit {

  publicationPackagesSubs: Subscription = new Subscription();
  packageId: any;
  currentPackageData: any;
  remainingPackagesData: any;
  bulletPoints: any;
  isLoaded: boolean = false;

  constructor(
    private utilityService: UtilityService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.queryParams.subscribe(params => {
      this.packageId = params['id'];
  });
   }

  ngOnInit(): void {
    this.publicationPackagesSubs = this.utilityService.getPublicationPackagesData().subscribe((response: any) => {
      if (response && response.data && response.data.length > 0) {
        let publicationPackagesData = response.data;
        this.currentPackageData = publicationPackagesData.filter((data:any) => data.id == this.packageId)[0];
        if(!this.currentPackageData) {
          window.location.href = '/404';
        }
        this.isLoaded = true;
        this.bulletPoints = Object.values(this.currentPackageData.attributes.BulletPoints);
        this.remainingPackagesData = publicationPackagesData.filter((data:any) => data.id != this.packageId);
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/publication/campaign/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }

  ngOnDestroy() {
    if(this.publicationPackagesSubs) {
      this.publicationPackagesSubs.unsubscribe();
    }
  }

  buyNow(id: number) {
    let checkoutLink = '/publication/auth/signup';
    this.router.navigate([checkoutLink], { queryParams: { id: id } });
  }
}
