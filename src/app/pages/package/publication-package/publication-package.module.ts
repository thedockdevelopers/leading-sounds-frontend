import { NgModule } from '@angular/core';
import { PublicationPackageRoutingModule } from './publication-package-routing.module';


@NgModule({
  declarations: [
  ],
  imports: [
    PublicationPackageRoutingModule
  ]
})
export class PublicationPackageModule { }
