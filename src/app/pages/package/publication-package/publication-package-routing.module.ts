import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { PublicationPackageComponent } from './publication-package.component';

const routes: Routes = [
  {
    path: '',
    component: PublicationPackageComponent
  }
];

@NgModule({
  declarations: [PublicationPackageComponent],
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule]
})
export class PublicationPackageRoutingModule { }
