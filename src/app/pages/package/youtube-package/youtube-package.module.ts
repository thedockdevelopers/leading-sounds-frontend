import { NgModule } from '@angular/core';
import { YoutubePackageRoutingModule } from './youtube-package-routing.module';


@NgModule({
  declarations: [
  ],
  imports: [
    YoutubePackageRoutingModule
  ]
})
export class YoutubePackageModule { }
