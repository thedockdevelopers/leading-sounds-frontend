import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { YoutubePackageComponent } from './youtube-package.component';

const routes: Routes = [
  {
    path: '',
    component: YoutubePackageComponent
  }
];

@NgModule({
  declarations: [YoutubePackageComponent],
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule]
})
export class YoutubePackageRoutingModule { }
