import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-instagram-package',
  templateUrl: './instagram-package.component.html',
  styleUrls: ['./instagram-package.component.scss']
})
export class InstagramPackageComponent implements OnInit {

  instaPackagesSubs: Subscription = new Subscription();
  packageId: any;
  currentPackageData: any;
  remainingPackagesData: any;
  bulletPoints: any;
  isLoaded: boolean = false;

  constructor(
    private utilityService: UtilityService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.queryParams.subscribe(params => {
      this.packageId = params['id'];
  });
   }

  ngOnInit(): void {
    this.instaPackagesSubs = this.utilityService.getInstagramPackagesData().subscribe((response: any) => {
      if (response && response.data && response.data.length > 0) {
        let instaPackagesData = response.data;
        this.currentPackageData = instaPackagesData.filter((data:any) => data.id == this.packageId)[0];
        if(!this.currentPackageData) {
          window.location.href = '/404';
        }
        this.isLoaded = true;
        this.bulletPoints = Object.values(this.currentPackageData.attributes.BulletPoints);
        this.remainingPackagesData = instaPackagesData.filter((data:any) => data.id != this.packageId);
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/instagram/campaign/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }

  ngOnDestroy() {
    if(this.instaPackagesSubs) {
      this.instaPackagesSubs.unsubscribe();
    }
  }

  buyNow(id: number) {
    let checkoutLink = '/instagram/auth/signup';
    this.router.navigate([checkoutLink], { queryParams: { id: id } });
  }

}
