import { NgModule } from '@angular/core';
import { InstagramPackageRoutingModule } from './instagram-package-routing.module';


@NgModule({
  declarations: [
  ],
  imports: [
    InstagramPackageRoutingModule
  ]
})
export class InstagramPackageModule { }
