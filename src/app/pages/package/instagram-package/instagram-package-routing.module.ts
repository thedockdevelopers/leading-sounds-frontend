import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { InstagramPackageComponent } from './instagram-package.component';

const routes: Routes = [
  {
    path: '',
    component: InstagramPackageComponent
  }
];

@NgModule({
  declarations: [InstagramPackageComponent],
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule]
})
export class InstagramPackageRoutingModule { }
