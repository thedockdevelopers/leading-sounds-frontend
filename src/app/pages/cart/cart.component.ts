import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  repostPackagesData: any;
  couponCode: any;
  isIncorrectCoupon: boolean = false;
  repostPackagesSubs: Subscription = new Subscription();
  totalAmount: number = 0;
  discountedAmount: number = 0;
  itemArrFromCookies: Array<any> = new Array<any>();
  cartItems: Array<any> = new Array<any>();
  noOfItem: number = 0;
  couponSpinner: boolean = false;
  cartConfig: any;
  isLoaded: boolean = false;

  constructor(
    private utilityService: UtilityService,
    private cookieService: CookieService,
    private router: Router) {
      this.cartItems.length = 0;
      this.utilityService.getCart().subscribe((response: any) => {
        if(response && response.data) {
          this.isLoaded = true;
          this.cartConfig = response.data.attributes;
        }
      });
     }

  ngOnInit(): void {
    this.itemArrFromCookies = this.cookieService.get('ls_rp_items_in_cart') ? JSON.parse(this.cookieService.get('ls_rp_items_in_cart')) : [];
    this.repostPackagesSubs = this.utilityService.getRepostPackagesData().subscribe((response: any) => {
      if (response && response.data && response.data.length > 0) {
        this.repostPackagesData = response.data;
        this.itemArrFromCookies.forEach((item: any) => {
          for(let i = 0; i < this.repostPackagesData.length; i++ ) {
            if(this.repostPackagesData[i].id === item.id) {
              this.repostPackagesData[i].BuyCounter = item.BuyCounter;
              this.noOfItem += item.BuyCounter;
              this.totalAmount += (this.repostPackagesData[i].attributes.Price*item.BuyCounter);
              let cartObj = {
                id: this.repostPackagesData[i].id,
                name: this.repostPackagesData[i].attributes.Name,
                price: this.repostPackagesData[i].attributes.Price,
                buyCounter: this.repostPackagesData[i].BuyCounter
              };
              this.cartItems.push(cartObj);
            }
          }
        });
        this.utilityService.cartSubject.next(this.noOfItem);
      }
    });
  }

  getFinalAmount() {
    return this.totalAmount-this.discountedAmount;
  }

  removeItem(id: number) {
    for(let i=0;i<this.cartItems.length; i++) {
      if(this.cartItems[i].id === id) {
        if(this.cartItems[i].buyCounter) {
          this.cartItems[i].buyCounter = this.cartItems[i].buyCounter - 1;
          this.repostPackagesData
          this.totalAmount -= this.cartItems[i].price;
          this.addItemObjToLS(id);
          this.noOfItem -= 1;
          this.utilityService.cartSubject.next(this.noOfItem);
          if(this.cartItems[i].buyCounter === 0) {
            this.cartItems.splice(i , 1);
          }
        }
        else {
          this.cartItems[i].buyCounter = 0;
        }
        break;
      }
    }
  }

  addItem(id: number) {
    for(let i=0;i<this.cartItems.length; i++) {
      if(this.cartItems[i].id === id) {
        this.cartItems[i].buyCounter = !this.cartItems[i].buyCounter ? 1 : this.cartItems[i].buyCounter + 1;
        this.totalAmount += this.cartItems[i].price;
        this.addItemObjToLS(id);
        this.noOfItem += 1;
        this.utilityService.cartSubject.next(this.noOfItem);
        break;
      }
    }
  }

  deleteItem(index: number, id: number) {
    this.noOfItem -= this.cartItems[index].buyCounter;
    this.totalAmount -= this.cartItems[index].price*this.cartItems[index].buyCounter;
    this.cartItems[index].buyCounter = 0;
    this.addItemObjToLS(id);
    this.cartItems.splice(index, 1);
    this.utilityService.cartSubject.next(this.noOfItem);
  }

  addItemObjToLS(id: number) {
    for(let i = 0; i< this.cartItems.length; i++) {
      if(this.cartItems[i].id === id) {
        for(let j = 0; j < this.itemArrFromCookies.length; j++) {
          if(this.itemArrFromCookies[j].id === id) {
            this.itemArrFromCookies.splice(j, 1);
          }
        }
        let itemObj = {
          id: id,
          BuyCounter: this.cartItems[i].buyCounter
        }
        if(itemObj.BuyCounter > 0) {
          this.itemArrFromCookies.push(itemObj);
        }
        this.cookieService.set('ls_rp_items_in_cart', JSON.stringify(this.itemArrFromCookies.sort((a, b) => a.id - b.id)), { expires: 2, sameSite: 'Lax', path: '/' });
        break;
      }
    }
    this.itemArrFromCookies = JSON.parse(this.cookieService.get('ls_rp_items_in_cart'));
    if(this.itemArrFromCookies.length == 0) {
      this.cookieService.delete('ls_rp_items_in_cart', '/');
    }
  }

  emptyCart() {
    this.cookieService.delete('ls_rp_items_in_cart', '/');
    this.cartItems = [];
    this.utilityService.cartSubject.next(0);
  }

  applyCoupon() {
    this.couponSpinner = true;
    let couponSpinner = document.getElementById('coupon-result');
    if(couponSpinner) {
      couponSpinner.style.visibility = 'hidden';
    }
    this.discountedAmount = 0;
    this.isIncorrectCoupon = false;
    this.utilityService.getCouponData(this.couponCode).subscribe((response: any) => {
      if(response) {
        this.couponSpinner = false;
        let couponSpinner = document.getElementById('coupon-result');
        if(couponSpinner) {
          couponSpinner.style.visibility = 'visible';
        }
        if(response.isValid) {
          this.discountedAmount = response.amountOff;
        }
        else {
          this.isIncorrectCoupon = true;
        }
      }
    })
  }

  cartCheckout() {
    let ls_cart_checkout = {
      cartArr: this.itemArrFromCookies,
      couponCode: this.couponCode
    };
    this.cookieService.set('ls_cart_checkout', JSON.stringify(ls_cart_checkout), { expires: 2, sameSite: 'Lax', path: '/' });
    let checkoutLink = '/cart/checkout';
    this.router.navigate([checkoutLink]);
  }
}
