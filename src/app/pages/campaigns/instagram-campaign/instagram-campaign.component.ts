import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-instagram-campaign',
  templateUrl: './instagram-campaign.component.html',
  styleUrls: ['./instagram-campaign.component.scss']
})
export class InstagramCampaignComponent implements OnInit {

  instaCampaignData: any;
  instaPackagesData: any;
  instaPackagesSubs: Subscription = new Subscription();

  constructor(
    private utilityService: UtilityService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.instaPackagesSubs = this.utilityService.getInstagramCampaign().subscribe((response: any) => {
      if (response && response.data) {
        this.instaCampaignData = response.data.attributes;
        this.instaPackagesData = response.data.attributes.Packages?.data;
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/instagram/campaign/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  ngOnDestroy() {
    if(this.instaPackagesSubs) {
      this.instaPackagesSubs.unsubscribe();
    }
  }
}
