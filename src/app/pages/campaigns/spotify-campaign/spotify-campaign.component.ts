import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-spotify-campaign',
  templateUrl: './spotify-campaign.component.html',
  styleUrls: ['./spotify-campaign.component.scss']
})
export class SpotifyCampaignComponent implements OnInit, OnDestroy {

  spotifyCampaignData: any;
  spotifyPackagesData: any;
  spotifyPackagesSubs: Subscription = new Subscription();

  constructor(
    private utilityService: UtilityService,
    private router: Router) { }

  ngOnInit(): void {
    this.spotifyPackagesSubs = this.utilityService.getSpotifyCampaign().subscribe((response: any) => {
      if (response && response.data) {
        this.spotifyCampaignData = response.data.attributes;
        this.spotifyPackagesData = response.data.attributes.Packages?.data;
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/spotify/campaign/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  ngOnDestroy() {
    if (this.spotifyPackagesSubs) {
      this.spotifyPackagesSubs.unsubscribe();
    }
  }

}
