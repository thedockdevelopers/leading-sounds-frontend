import { NgModule } from '@angular/core';
import { SpotifyCampaignRoutingModule } from './spotify-campaign-routing.module';



@NgModule({
  declarations: [
  ],
  imports: [
    SpotifyCampaignRoutingModule
  ]
})
export class SpotifyCampaignModule { }
