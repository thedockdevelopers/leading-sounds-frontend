import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { YoutubeCampaignComponent } from './youtube-campaign.component';
import { SharedModule } from '../../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: YoutubeCampaignComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule],
  declarations: [YoutubeCampaignComponent]
})
export class YoutubeCampaignRoutingModule { }
