import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-youtube-campaign',
  templateUrl: './youtube-campaign.component.html',
  styleUrls: ['./youtube-campaign.component.scss']
})
export class YoutubeCampaignComponent implements OnInit {

  ytCampaignData: any;
  ytPackagesData: any;
  ytPackagesSubs: Subscription = new Subscription();

  constructor(
    private utilityService: UtilityService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.ytPackagesSubs = this.utilityService.getYtCampaign().subscribe((response: any) => {
      if (response && response.data) {
        this.ytCampaignData = response.data.attributes;
        this.ytPackagesData = response.data.attributes.Packages?.data;
      }
    });
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  openPackage(id: number) {
    let packageLink = '/youtube/campaign/package';
    this.router.navigate([packageLink], { queryParams: { id: id } });
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  ngOnDestroy() {
    if (this.ytPackagesSubs) {
      this.ytPackagesSubs.unsubscribe();
    }
  }
}
