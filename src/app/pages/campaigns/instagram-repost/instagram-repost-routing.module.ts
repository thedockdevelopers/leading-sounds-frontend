import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { InstagramRepostComponent } from './instagram-repost.component';
import { CookieService  } from 'ngx-cookie-service';
import { SharedModule } from '../../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: InstagramRepostComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule],
  declarations: [InstagramRepostComponent],
  providers: [CookieService]
})
export class InstagramRepostRoutingModule { }
