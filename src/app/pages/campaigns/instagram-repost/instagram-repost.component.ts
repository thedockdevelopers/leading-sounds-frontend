import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-instagram-repost',
  templateUrl: './instagram-repost.component.html',
  styleUrls: ['./instagram-repost.component.scss']
})
export class InstagramRepostComponent implements OnInit {

  repostData: any;
  repostPackagesData: any;
  repostPackagesSubs: Subscription = new Subscription();
  totalAmount: number = 0;
  itemArr: Array<any> = new Array<any>();
  noOfItem: number = 0;
  stepsData: any;

  constructor(
    private utilityService: UtilityService,
    private cookieService: CookieService) { }

  ngOnInit(): void {
    this.itemArr = this.cookieService.get('ls_rp_items_in_cart') ? JSON.parse(this.cookieService.get('ls_rp_items_in_cart')) : [];
    this.repostPackagesSubs = this.utilityService.getInstagramRepost().subscribe((response: any) => {
      if (response && response.data) {
        this.repostData = response.data.attributes;
        this.repostPackagesData = response.data.attributes.Packages?.data;
        this.stepsData = response.data.attributes.Steps?.data.slice(0,3);
        this.itemArr.forEach((item: any) => {
          for(let i = 0; i < this.repostPackagesData.length; i++ ) {
            if(this.repostPackagesData[i].id === item.id) {
              this.repostPackagesData[i].BuyCounter = item.BuyCounter;
              this.totalAmount += (this.repostPackagesData[i].attributes.Price*item.BuyCounter);
              this.noOfItem += item.BuyCounter;
              this.utilityService.cartSubject.next(this.noOfItem);
            }
          }
        });
      }
    });
  }

  removeItem(id: number) {
    for(let i=0;i<this.repostPackagesData.length; i++) {
      if(this.repostPackagesData[i].id === id) {
        if(this.repostPackagesData[i].BuyCounter) {
          this.repostPackagesData[i].BuyCounter = this.repostPackagesData[i].BuyCounter - 1;
          this.totalAmount -= this.repostPackagesData[i].attributes.Price;
          this.noOfItem -= 1;
          this.utilityService.cartSubject.next(this.noOfItem);
          this.addItemObjToLS(id);
        }
        else {
          this.repostPackagesData[i].BuyCounter = 0;
        }
        break;
      }
    }
  }

  addItem(id: number) {
    for(let i=0;i<this.repostPackagesData.length; i++) {
      if(this.repostPackagesData[i].id === id) {
        this.repostPackagesData[i].BuyCounter = !this.repostPackagesData[i].BuyCounter ? 1 : this.repostPackagesData[i].BuyCounter + 1;
        this.totalAmount += this.repostPackagesData[i].attributes.Price;
        this.noOfItem += 1;
        this.utilityService.cartSubject.next(this.noOfItem);
        this.addItemObjToLS(id);
        break;
      }
    }
  }

  getFrequency(frequency: string) {
    return this.utilityService.getFrequency(frequency);
  }

  getBulletPoints(pointsObj: any) {
    return Object.values(pointsObj);
  }

  getCount(count: any) {
    return (!count ? 0 : count);
  }

  addItemObjToLS(id: number) {
    for(let i = 0; i< this.repostPackagesData.length; i++) {
      if(this.repostPackagesData[i].id === id) {
        for(let j = 0; j < this.itemArr.length; j++) {
          if(this.itemArr[j].id === id) {
            this.itemArr.splice(j, 1);
          }
        }
        let itemObj = {
          id: id,
          BuyCounter: this.repostPackagesData[i].BuyCounter
        }
        if(itemObj.BuyCounter > 0) {
          this.itemArr.push(itemObj);
        }
        this.cookieService.set('ls_rp_items_in_cart', JSON.stringify(this.itemArr.sort((a, b) => a.id - b.id)), { expires: 2, sameSite: 'Lax', path: '/' });
        break;
      }
    }
    this.itemArr = JSON.parse(this.cookieService.get('ls_rp_items_in_cart'));
    if(this.itemArr.length == 0) {
      this.cookieService.delete('ls_rp_items_in_cart', '/');
    }
  }
  
  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  ngOnDestroy() {
    if(this.repostPackagesSubs) {
      this.repostPackagesSubs.unsubscribe();
    }
  }

}
