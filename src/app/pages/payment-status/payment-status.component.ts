import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.scss']
})
export class PaymentStatusComponent implements OnInit {

  paymentSuccess: boolean = false;
  transactionId: string | undefined;
  isLoaded: boolean = false;
  invoiceUrl: any;

  constructor(
    private utilityService: UtilityService,
    private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.transactionId = params['id'];
    });
  }

  ngOnInit(): void {
    this.utilityService.getPaymentStatus(this.transactionId).subscribe((response: any) => {
      if(response) {
        this.isLoaded = true;
        if (response.success) {
          this.paymentSuccess = true;
          this.invoiceUrl = response.invoice;
        }
        else if(response.disputed) {
          this.paymentSuccess = false;
        }
        else {
          //window.location.href = '/';
        }
      }
      
    }, (error: any) => {
      //window.location.href = '/';
    })

  }
}
