import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { PaymentStatusComponent } from './payment-status.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentStatusComponent
  }
];

@NgModule({
  declarations: [PaymentStatusComponent],
  imports: [RouterModule.forChild(routes), CommonModule, SharedModule],
  exports: [RouterModule]
})
export class PaymentStatusRoutingModule { }
