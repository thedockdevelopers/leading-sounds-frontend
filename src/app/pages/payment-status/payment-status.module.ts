import { NgModule } from '@angular/core';
import { PaymentStatusRoutingModule } from './payment-status-routing.module';


@NgModule({
  declarations: [
    
  ],
  imports: [
    PaymentStatusRoutingModule
  ]
})
export class PaymentStatusModule { }
