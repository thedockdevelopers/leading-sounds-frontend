export class UserInfoModel {
    firstName: string | undefined;
    lastName: string | undefined ;
    email: string | undefined ;
    phone: number | undefined;
    address: string | undefined;
    city: string | undefined;
    state: string | undefined;
    zipCode: string | undefined;
    country: string | undefined;
    instaUsername?: string | undefined;
    spotifyUserName?: string | undefined;
    youtubeChannelLink?: string | undefined;
}