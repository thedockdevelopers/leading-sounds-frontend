import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderModule } from './components/loader/loader.module';
import { ImagePipe } from './pipes/image.pipe';

@NgModule({
  declarations: [
    ImagePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [ LoaderModule, ImagePipe]
})
export class SharedModule { }