import { Pipe, PipeTransform } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

 @Pipe({
 name: 'imagepipe'
  })
 export class ImagePipe implements PipeTransform {

    constructor(
        private utilityService: UtilityService
    ) {

    }

  transform(url: string): any {
        let mediaUrl: any = this.utilityService.getBaseUrl() + url
        return mediaUrl;
   }
 }