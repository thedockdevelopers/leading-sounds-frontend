import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  private globalConfigSubject = new BehaviorSubject<any>(null);
  globalConfigObservable: Observable<any> = this.globalConfigSubject.asObservable();

  private loadedSubject = new BehaviorSubject<any>(null);
  loadedObservable: Observable<any> = this.loadedSubject.asObservable();
  
  cartSubject = new BehaviorSubject<any>(null);
  cartObservable: Observable<any> = this.cartSubject.asObservable();
  
  globalConfigs: any;
  homePageData: any;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) { }

  getFrequency(frequency: string) {
    switch (frequency) {
      case 'PerWeek':
        return 'Per Week';
      case 'PerMonth':
        return 'Per Month'
      case 'PerYear':
        return 'Per Year';
      default:
        return 'One Time';
    }
  }

  getBaseUrl() {
    return 'http://localhost:1337';
  }

  getApiBaseUrl() {
    return 'http://localhost:1337/api/';
  }

  getGlobalConfigurations() {
    let globalConfigApiUrl = this.getApiBaseUrl() + 'global-configuration?populate=*';
    this.http.get<any>(globalConfigApiUrl).subscribe((response: any) => {
      if (response && response.data && response.data.attributes) {
        this.globalConfigSubject.next(true);
        this.setGlobalConfig(response.data.attributes);
      }
    })
  }

  getPublishableKey(): string {
    return this.globalConfigs.StripePublishableKey;
  }

  setGlobalConfig(globalConfigs: any) {
    this.globalConfigs = globalConfigs;
  }

  getGlobalConfig() {
    return this.globalConfigs;
  }

  getTeaserData(): Observable<any> {
    let teaserApiUrl = this.getApiBaseUrl() + 'teasers?populate=*';
    return this.http.get<any>(teaserApiUrl);
  }

  getPartnersData(): Observable<any> {
    let partnerApiUrl = this.getApiBaseUrl() + 'partners?populate=*';
    return this.http.get<any>(partnerApiUrl);
  }

  getAboutUsData(): Observable<any> {
    let aboutUsApiUrl = this.getApiBaseUrl() + 'about-uses?populate=*';
    return this.http.get<any>(aboutUsApiUrl);
  }

  getServiceData(): Observable<any> {
    let serviceApiUrl = this.getApiBaseUrl() + 'services?populate=*';
    return this.http.get<any>(serviceApiUrl);
  }

  getWhyPromoteData(): Observable<any> {
    let whyPromoteApiUrl = this.getApiBaseUrl() + 'why-promotes';
    return this.http.get<any>(whyPromoteApiUrl);
  }

  getTestimonialsData(): Observable<any> {
    let testimonialsApiUrl = this.getApiBaseUrl() + 'testimonials?populate=*';
    return this.http.get<any>(testimonialsApiUrl);
  }

  getFaqsData(): Observable<any> {
    let faqsApiUrl = this.getApiBaseUrl() + 'faqs';
    return this.http.get<any>(faqsApiUrl);
  }

  getInstagramPackagesData(): Observable<any> {
    let instaPackagesApiUrl = this.getApiBaseUrl() + 'instagram-packages?populate=*';
    return this.http.get<any>(instaPackagesApiUrl);
  }
  
  getSpotifyPackagesData(): Observable<any> {
    let spotifyPackagesApiUrl = this.getApiBaseUrl() + 'spotify-packages?populate=*';
    return this.http.get<any>(spotifyPackagesApiUrl);
  }
  
  getYTPackagesData(): Observable<any> {
    let ytPackagesApiUrl = this.getApiBaseUrl() + 'youtube-packages?populate=*';
    return this.http.get<any>(ytPackagesApiUrl);
  }

  getPublicationPackagesData(): Observable<any> {
    let publicationPackagesApiUrl = this.getApiBaseUrl() + 'publication-packages?populate=*';
    return this.http.get<any>(publicationPackagesApiUrl);
  }
  
  getRepostPackagesData(): Observable<any> {
    let repostPackagesApiUrl = this.getApiBaseUrl() + 'repost-packages?populate=*';
    return this.http.get<any>(repostPackagesApiUrl);
  }

  getCouponData(couponCode: string): Observable<any> {
    let couponApiUrl = this.getApiBaseUrl() + 'stripe-coupon/' + couponCode;
    return this.http.get<any>(couponApiUrl);
  }

  postPaymentData(paymentObj: any): Observable<any> {
    let processPayApiUrl = this.getApiBaseUrl() + 'stripe-payment';
    return this.http.post<any>(processPayApiUrl, {paymentObj: paymentObj});
  }

  postPublicationContactData(clientObj: any): Observable<any> {
    let publicationContactApiUrl = this.getApiBaseUrl() + 'publication-contact';
    return this.http.post<any>(publicationContactApiUrl, {clientObj: clientObj});
  }

  postContactUsData(clientObj: any): Observable<any> {
    let contactUsApiUrl = this.getApiBaseUrl() + 'post-contact';
    return this.http.post<any>(contactUsApiUrl, {clientObj: clientObj});
  }

  getPaymentStatus(transactionId: any): Observable<any> {
    let paymentStatusApiUrl = this.getApiBaseUrl() + 'payment-status';
    return this.http.post<any>(paymentStatusApiUrl, {transactionId: transactionId});
  }

  getOurWorkData(): Observable<any> {
    let beforeAfterApiUrl = this.getApiBaseUrl() + 'before-afters?populate=*';
    return this.http.get<any>(beforeAfterApiUrl);
  }

  getCart(): Observable<any> {
    let cartConfigApiUrl = this.getApiBaseUrl() + 'cart-page?populate=*';
    return this.http.get<any>(cartConfigApiUrl);
  }

  setUpCartItems() {
    let itemArrFromCookies = this.cookieService.get('ls_rp_items_in_cart') ? JSON.parse(this.cookieService.get('ls_rp_items_in_cart')) : [];
    this.getRepostPackagesData().subscribe((response: any) => {
      let noOfItem = 0;
      if (response && response.data && response.data.length > 0) {
        let repostPackagesData = response.data;
        itemArrFromCookies.forEach((item: any) => {
          for(let i = 0; i < repostPackagesData.length; i++ ) {
            if(repostPackagesData[i].id === item.id) {
              noOfItem += item.BuyCounter;
            }
          }
        });
        this.cartSubject.next(noOfItem);
      }
    });
  }

  getInstagramCampaign(): Observable<any> {
    let instaCampaignApiUrl = this.getApiBaseUrl() + 'instagram-campaign-page?populate=*';
    return this.http.get<any>(instaCampaignApiUrl);
  }

  getSpotifyCampaign(): Observable<any> {
    let spotifyCampaignApiUrl = this.getApiBaseUrl() + 'spotify-campaign-page?populate=*';
    return this.http.get<any>(spotifyCampaignApiUrl);
  }

  getYtCampaign(): Observable<any> {
    let ytCampaignApiUrl = this.getApiBaseUrl() + 'youtube-campaign-page?populate=*';
    return this.http.get<any>(ytCampaignApiUrl);
  }
  
  getInstagramRepost(): Observable<any> {
    let instaRepostApiUrl = this.getApiBaseUrl() + 'instagram-repost-page?populate=*';
    return this.http.get<any>(instaRepostApiUrl);
  }

  getPublication(): Observable<any> {
    let publicationApiUrl = this.getApiBaseUrl() + 'publication-page?populate=*';
    return this.http.get<any>(publicationApiUrl);
  }

  getAboutUs(): Observable<any> {
    let aboutUsApiUrl = this.getApiBaseUrl() + 'about-us-page?populate=*';
    return this.http.get<any>(aboutUsApiUrl);
  }

  getHomePage(): Observable<any> {
    let homePageApiUrl = this.getApiBaseUrl() + 'home-page?populate=*';
    return this.http.get<any>(homePageApiUrl);
  }

  setHomePageData(data: any) {
    this.homePageData = data;
  }

  getHomePageData(): any {
    return this.homePageData;
  }

}
