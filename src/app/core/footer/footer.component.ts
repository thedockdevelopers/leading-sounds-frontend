import { Component, OnInit } from '@angular/core';
import { UtilityService } from '../services';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  globalConfig: any;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit(): void {
    this.globalConfig = this.utilityService.getGlobalConfig();
  }

}
