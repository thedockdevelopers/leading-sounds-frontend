import { Component, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { Router } from '@angular/router';
import { UtilityService } from '../services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  //encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  @ViewChildren(MatMenuTrigger) trigger: QueryList<MatMenuTrigger>;
  cartItemCount: number = 0;
  displayCartIcon: boolean = false;
  globalConfig: any;

  
  constructor(
    private utilityService: UtilityService,
    private router: Router
  ) {
    if (!window.location.href.includes('/auth/signup')) {
      this.displayCartIcon = true;
    }
  }

  openMyMenu(indexOfMenu: number) {
    this.closeMyMenu();
    this.trigger.toArray()[indexOfMenu].openMenu();
    // setTimeout(() => {
    //   this.trigger.toArray()[indexOfMenu].closeMenu();
    // }, 10000)
  }

  closeMyMenu() {
    this.trigger.toArray().forEach((menu: any) => {
      menu.closeMenu();
    })
  }

  ngOnInit(): void {
    this.globalConfig = this.utilityService.getGlobalConfig();
    this.utilityService.cartObservable.subscribe((itemCount: number) => {
      this.cartItemCount = itemCount;
    });
  }

  goToHome() {
    window.location.href = '/';
  }

  ngAfterViewInit() {
    let navLinks = document.getElementsByClassName("nav-link");
    for (var i = 0; i < navLinks.length; i++) {
      navLinks[i].addEventListener('click', this.closeMobileMenu, false);
  }
  }

  openMobileMenu() {
    let menu: any = document.getElementById('mobile-menu');
    menu.style.display = 'inline';
  }

  closeMobileMenu() {
    let menu: any = document.getElementById('mobile-menu');
    menu.style.display = 'none';
  }
}
