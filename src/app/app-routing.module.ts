import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'cart',
    children: [
      {
        path: 'checkout',
        loadChildren: () => import('./pages/checkout/cart-checkout/cart-checkout.module').then(m => m.CartCheckoutModule),
      },
      {
        path: '',
        loadChildren: () => import('./pages/cart/cart.module').then(m => m.CartModule),
      }
    ]
  },
  {
    path: 'payment-status',
    loadChildren: () => import('./pages/payment-status/payment-status.module').then(m => m.PaymentStatusModule),
  },
  {
    path: 'publication',
    children: [
      {
        path: 'package',
        loadChildren: () => import('./pages/package/publication-package/publication-package.module').then(m => m.PublicationPackageModule)
      },
      {
        path: 'auth/signup',
        loadChildren: () => import('./pages/checkout/publication-checkout/publication-checkout.module').then(m => m.PublicationCheckoutModule)
      },
      {
        path: '',
        loadChildren: () => import('./pages/publication/publication.module').then(m => m.PublicationModule)
      }
    ]
  },
  {
    path: 'about-us',
    loadChildren: () => import('./pages/about-us/about-us.module').then(m => m.AboutUsModule),
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./pages/contact-us/contact-us.module').then(m => m.ContactUsModule),
  },
  {
    path: 't&c',
    loadChildren: () => import('./pages/t-and-c/t-and-c.module').then(m => m.TAndCModule),
  },
  {
    path: 'faqs',
    loadChildren: () => import('./pages/faqs/faqs.module').then(m => m.FaqsModule),
  },
  {
    path: 'instagram',
    children: [
      {
        path: 'campaign',
        children: [
          {
            path: 'package',
            loadChildren: () => import('./pages/package/instagram-package/instagram-package.module').then(m => m.InstagramPackageModule)
          },
          {
            path: '',
            loadChildren: () => import('./pages/campaigns/instagram-campaign/instagram-campaign.module').then(m => m.InstagramCampaignModule)
          }
        ]
      },
      {
        path: 'repost',
        loadChildren: () => import('./pages/campaigns/instagram-repost/instagram-repost.module').then(m => m.InstagramRepostModule)
      },
      {
        path: 'auth/signup',
        loadChildren: () => import('./pages/checkout/instagram-checkout/instagram-checkout.module').then(m => m.InstagramCheckoutModule)
      }
    ]
  },
  {
    path: 'youtube',
    children: [
      {
        path: 'campaign',
        children: [
          {
            path: 'package',
            loadChildren: () => import('./pages/package/youtube-package/youtube-package.module').then(m => m.YoutubePackageModule)
          },
          {
            path: '',
            loadChildren: () => import('./pages/campaigns/youtube-campaign/youtube-campaign.module').then(m => m.YoutubeCampaignModule)
          }
        ]
      },
      {
        path: 'auth/signup',
        loadChildren: () => import('./pages/checkout/youtube-checkout/youtube-checkout.module').then(m => m.YoutubeCheckoutModule)
      }
    ]
  },
  {
    path: 'spotify',
    children: [
      {
        path: 'campaign',
        children: [
          {
            path: 'package',
            loadChildren: () => import('./pages/package/spotify-package/spotify-package.module').then(m => m.SpotifyPackageModule)
          },
          {
            path: '',
            loadChildren: () => import('./pages/campaigns/spotify-campaign/spotify-campaign.module').then(m => m.SpotifyCampaignModule)
          }
        ]
      },
      {
        path: 'auth/signup',
        loadChildren: () => import('./pages/checkout/spotify-checkout/spotify-checkout.module').then(m => m.SpotifyCheckoutModule)
      }
    ]
  },
  {
    path: '404',
    loadChildren: () => import('./pages/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false, anchorScrolling: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
