import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  aboutUsData: any;
  feedData: any;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.feedData = this.utilityService.getHomePageData();
    this.aboutUsData = this.feedData.AboutUs.data;
  }

}
