import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurWorkComponent } from './our-work.component';



@NgModule({
  declarations: [
    OurWorkComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [OurWorkComponent]
})
export class OurWorkModule { }
