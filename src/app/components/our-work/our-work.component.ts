import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-our-work',
  templateUrl: './our-work.component.html',
  styleUrls: ['./our-work.component.scss']
})
export class OurWorkComponent implements OnInit {

  feedData: any;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.feedData = this.utilityService.getHomePageData();
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

}
