import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  videoUrl: any;

  constructor(
    private utilityService: UtilityService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    let feedData = this.utilityService.getHomePageData();
    let url = this.utilityService.getBaseUrl() + feedData.Video.data.attributes.url;
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
