import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqsItemComponent } from './faqs-item.component';

@NgModule({
  declarations: [
    FaqsItemComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FaqsItemComponent
  ]
})
export class FaqsItemModule { }
