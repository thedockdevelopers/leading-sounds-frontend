import { Component, Input, OnChanges } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-faqs-item',
  templateUrl: './faqs-item.component.html',
  animations: [
    trigger('slideInOut', [
      state('expanded', style({
        overflow: 'hidden',
        height: '*'
      })),
      state('not-expanded', style({
        opacity: '0',
        overflow: 'hidden',
        height: '0px'
      })),
      transition('expanded => not-expanded', animate('800ms ease-out')),
      transition('not-expanded => expanded', animate('800ms ease-out'))
    ])
  ],
  styleUrls: ['./faqs-item.component.scss'],
})
export class FaqsItemComponent implements OnChanges {
  @Input() data: any;

  faqDataArray: Array<any> = new Array<any>();

  constructor() { }

  ngOnChanges() {
    if (this.data && this.data.length > 0) {
      for (let i = 0; i < this.data.length; i++) {
        let faqObj = {
          question: this.data[i].attributes.Question,
          answer: this.data[i].attributes.Answer,
          expanded: 'not-expanded'
        };
        this.faqDataArray.push(faqObj);
      }
    }
  }

  faqClicked(index: number) {
    for (let i = 0; i < this.faqDataArray.length; i++) {
      if (i == index) {
        this.faqDataArray[index].expanded = this.faqDataArray[index].expanded === 'not-expanded' ? 'expanded' : 'not-expanded';
      }
      else {
        this.faqDataArray[i].expanded = 'not-expanded';
      }
    }
  }
}
