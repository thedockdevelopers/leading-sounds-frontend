import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeaserComponent } from './teaser.component';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  declarations: [
    TeaserComponent
  ],
  imports: [
    CommonModule,
    SwiperModule
  ],
  exports: [TeaserComponent]
})
export class TeaserModule { }
