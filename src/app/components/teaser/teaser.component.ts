import { ViewportScroller } from '@angular/common';
import { ChangeDetectorRef, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';
import { UtilityService } from 'src/app/core/services';
import { SwiperOptions } from 'swiper';
import SwiperCore, { EffectFade, Navigation, Pagination, Autoplay } from "swiper";
SwiperCore.use([EffectFade, Navigation, Pagination, Autoplay]);

@Component({
  selector: 'app-teaser',
  templateUrl: './teaser.component.html',
  styleUrls: ['./teaser.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TeaserComponent implements OnInit {
  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 0,
    navigation: true,
    pagination: false,
    scrollbar: { draggable: false },
    observer: true,
    observeParents: true,
    parallax:true,
    effect: 'fade',
    loop: true,
    autoplay: {
      delay: 5000,
      },
  };
  teaserData: any;

  constructor(private cdr: ChangeDetectorRef,
    private viewportScroller: ViewportScroller,
    private utilityService: UtilityService) {}

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    let feedData = this.utilityService.getHomePageData();
    this.teaserData = feedData.Teasers.data;
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  openLink(link: string) {
    window.location.href = link;
  }

  scrollToPartners(id: string) {
    //this.viewportScroller.scrollToAnchor(id);
    this.viewportScroller.scrollToPosition([0, window.innerHeight-25])
  }
}
