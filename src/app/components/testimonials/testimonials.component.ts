import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {

  testimonialsData: any;
  feedData: any;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.feedData = this.utilityService.getHomePageData();
    this.testimonialsData = this.feedData.Testimonials.data;
  }

}
