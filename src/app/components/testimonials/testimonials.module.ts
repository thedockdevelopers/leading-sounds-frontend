import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestimonialsComponent } from './testimonials.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    TestimonialsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [TestimonialsComponent]
})
export class TestimonialsModule { }
