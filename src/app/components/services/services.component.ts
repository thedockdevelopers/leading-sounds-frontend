import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  feedData: any;
  servicesData: any;
  bgImage: any;

  constructor(
    private utilityService: UtilityService,
    private router: Router) { }

  ngOnInit(): void {
    this.feedData = this.utilityService.getHomePageData();
    this.servicesData = this.feedData.Services.data;
  }

  openService(link: string) {
    window.location.href = link;
  }

  getImageUrl(url: string) {
    return this.utilityService.getBaseUrl() + url;
  }

  openAboutUs() {
    this.router.navigate(['/about-us']);
  }

}
