import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-why-promote',
  templateUrl: './why-promote.component.html',
  styleUrls: ['./why-promote.component.scss']
})
export class WhyPromoteComponent implements OnInit {

  whyPromoteData: any;
  feedData: any;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.feedData = this.utilityService.getHomePageData();
    this.whyPromoteData = this.feedData.WhyPromote.data;
  }

}
