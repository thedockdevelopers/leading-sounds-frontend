import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { WhyPromoteComponent } from './why-promote.component';

@NgModule({
  declarations: [
    WhyPromoteComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ],
  exports: [WhyPromoteComponent]
})
export class WhyPromoteModule { }
