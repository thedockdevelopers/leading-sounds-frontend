import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { UtilityService } from 'src/app/core/services';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  partnersData: any;
  feedData: any;
  carouselConfig: NguCarouselConfig = {
    grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
    load: 3,
    interval: {timing: 1000, initialDelay: 1000},
    loop: true,
    touch: true,
    velocity: 0.1,
    point: {
      visible: false,
      hideOnSingleSlide: true
    }
  }

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.feedData = this.utilityService.getHomePageData();
    this.partnersData = this.feedData.Partners.data;
  }

  openPartner(link: string) {
    window.location.href = link;
  }
}
