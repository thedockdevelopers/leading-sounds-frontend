import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguCarouselModule } from '@ngu/carousel';
import { PartnersComponent } from './partners.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    PartnersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NguCarouselModule
  ],
  exports: [PartnersComponent]
})
export class PartnersModule { }
